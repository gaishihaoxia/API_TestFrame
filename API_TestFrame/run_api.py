#encoding:utf-8
import requests,re
import time
import logging,os
from nt import chdir
import xlrd,hashlib,json
import sys
reload(sys)
sys.setdefaultencoding('utf-8')



def runTest(testCaseFile):

    log_file = os.path.join(os.path.dirname(os.getcwd()),'log\\%s.log'%os.path.splitext(testCaseFile)[0])

    log_format = '[%(asctime)s][%(levelname)s]%(message)s'
        
    logging.basicConfig(format=log_format,filename=log_file,filemode='a+',level=logging.DEBUG)


    testCaseFile = os.path.join(os.getcwd(),testCaseFile)
    if not os.path.exists(testCaseFile):
        logging.error('测试用例文件不存在')
        sys.exit()
    testCase =xlrd.open_workbook(testCaseFile)
    table = testCase.sheet_by_index(0) #得到第一张表格
    errorCase = []


    
    error_num = 0  #
    
    for i in range(1,table.nrows):  #从第一行开始读取数据
        if table.cell(i,9).value.replace('\n','').replace('\r','') != "Yes":  #table.cell 表示单元格
            continue
        num = str(int(table.cell(i,0).value)).replace('\n','').replace('\r','')
        api_purpose = table.cell(i,1).value.replace('\n','').replace('\r','')
        api_host = table.cell(i,2).value.replace('\n','').replace('\r','')
        request_url = table.cell(i,3).value.replace('\n','').replace('\r','')
        request_method = table.cell(i,4).value.replace('\n','').replace('\r','')
        request_data = table.cell(i,6).value.replace('\n','').replace('\r','')
        encryption = table.cell(i,7).value.replace('\n','').replace('\r','')
        check_point = table.cell(i,8).value
        api_proxy = table.cell(i,10).value.replace('\n','').replace('\r','')
        
        request_data = json.loads(request_data)   
           


        status,resp = interfaceTest(num,api_purpose,api_host,request_url,request_data,check_point,request_method,api_proxy)
        
        if status != 200 or check_point not in resp:
            chdir('D:/Users/Desktop/spider_man/API_TestFrame/error_result')
            f=open('error_result.txt','a+')
            get_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime())
            error_info = str(status)+'----'+get_time+'----'+num+'----'+api_purpose+'----'+api_host+'----'+request_url
    
            f.write('=============================================================================\n')
            f.write(error_info+'\n')
            error_num = error_num+1
            
    
    f.write(u'失败的用例共有'+str(error_num)+u"条")        
    f.close()       
#=================================================================================================================================
                   

            
def interfaceTest(num,api_purpose,api_host,request_url,request_data,check_point,request_method,api_proxy):
    
    headers = {'Content-Type':'text/html;charset=GB18030',
                'Connection':'Keep-Alive',
                'Referer':'http://'+api_host,
                'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'
    }
    
    api_proxys =eval(api_proxy)  #要把字符串转化为字典类型


        
    if request_method == "GET":
        r =requests.get(url='http://'+api_host+request_url,params=request_data,headers=headers,proxies=api_proxys)
    
    elif request_method == "POST":
        r = requests.post(url='http://'+api_host+request_url,params=request_data,headers=headers,proxies=api_proxys)
       
            
    status = r.status_code
    resp = r.text

    if status == 200:
        if re.search(check_point,resp):
            print 'found it'
            logging.info(num+' '+api_purpose + "-----成功，"+str(status)+'\n')
            return status,resp
        else: 
            print 'not found'
            logging.error(num+' '+api_purpose + "-----失败！！！["+str(status)+']')
            return 200,resp
 
    else:
        print status
        logging.info('\n'+api_purpose+'响应码不是200')


if __name__ =="__main__":
    os.chdir('cases_xlsx')
    get_casefile = os.listdir(os.getcwd())
    for f in get_casefile:
        runTest(f)
        



